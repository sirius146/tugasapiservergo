package models

type Mahasiswa struct {
	ID       int    `json:"id"`
	NIM      int    `json:"nim"`
	Name     string `name:"name"`
	Semester int    `json:"semester"`
}

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    []Mahasiswa
}
