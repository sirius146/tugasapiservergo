package mahasiswa

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"apigov2/config"
	"apigov2/models"
)

const (
	table          = "mahasiswa"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {

	var mahasiswas []models.Mahasiswa

	db, err := config.MYSQL()

	if err != nil {
		log.Fatal("Cant Connect to MYSQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v Order By id DESC", table)

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa models.Mahasiswa

		if err = rowQuery.Scan(&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester); err != nil {
			return nil, err
		}

		if err != nil {
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)
	}

	return mahasiswas, nil

}

func Insert(ctx context.Context, mhs models.Mahasiswa) error {

	db, err := config.MYSQL()

	if err != nil {
		log.Fatal("Can't Connect to MYSQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (nim,nama,semester) values (%v, '%v', %v)", table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester)

	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, mhs models.Mahasiswa) error {

	db, err := config.MYSQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nim = %d, nama ='%s', semester = %d where nim = '%d'",
		table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		mhs.NIM,
	)
	fmt.Println(queryText)

	result, err := db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	rows, err := result.RowsAffected()
	fmt.Println("rows")
	if rows == 0 {
		fmt.Println("ID tidak ditemukan")
		return errors.New("ID tidak ditemukan")
	}

	return nil
}

func Delete(ctx context.Context, mhs models.Mahasiswa) error {

	db, err := config.MYSQL()

	if err != nil {
		log.Fatal("Cant Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, mhs.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("ID tidak ada")
	}

	return nil
}
